﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using Coding4Fun.Toolkit.Controls;
using Microsoft.PlayerFramework;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MyCanal2WithHLS_SDK
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        //    MediaExtensionManager mediaExtMgr;
        //    HLSControllerFactory controllerFactory;
        //    IHLSController controller;
        Windows.System.Display.DisplayRequest KeepScreenOnRequest = new Windows.System.Display.DisplayRequest();
        //private SimpleOrientationSensor Sensore;


        public MainPage()
        {
            this.InitializeComponent();

            this.NavigationCacheMode = NavigationCacheMode.Required;

            KeepScreenOnRequest.RequestActive();

        }



        public async void ShowWelcome()
        {
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (settings.Values.Keys.Contains(App.Message))
            {
                var content = settings.Values[App.Message] as string;

                MessageDialog mydial = new MessageDialog(content);
                mydial.Title = "Cameroon O bossoooo";
                mydial.Commands.Add(new UICommand(
                    "OK",
                    new UICommandInvokedHandler(this.CommandInvokedHandler_noclick)));
                
                await mydial.ShowAsync();

            }
        }

        private void MainPage_OnSizeChanged(object sender, SizeChangedEventArgs e)
        {
            string CurrentViewState = ApplicationView.GetForCurrentView().Orientation.ToString();

            if (CurrentViewState == "Portrait")
            {
                AdMediator_2D5EBB.Visibility = Visibility.Visible;
                //BottomAppBar.IsOpen = true;
            }

            if (CurrentViewState == "Landscape")
            {
                GoogleAnalytics.EasyTracker.GetTracker().SendEvent("Layout Change", "wide screen", "", 0);

                AdMediator_2D5EBB.Visibility = Visibility.Collapsed;
            }

        }

        public async void reviewfunction()
        {

            //For windows phone 8.1 app or universal app use the following line of code
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;


            //set the app name
            string Appname = "My Canal 2";

            if (!settings.Values.ContainsKey("review"))
            {
                settings.Values.Add("review", 1);
                settings.Values.Add("rcheck", 0);
            }
            else
            {
                int no = Convert.ToInt32(settings.Values["review"]);
                int check = Convert.ToInt32(settings.Values["rcheck"]);
                no++;
                if ((no == 3 || no == 6 || no % 10 == 0) && check == 0)
                {
                    settings.Values["review"] = no;
                    MessageDialog mydial = new MessageDialog(@"Si toi aussi tu ya vraiment Mo, put fap etoiles la-bas!

Show us some love on the store and help us improving the app,so we can include more features");
                    mydial.Title = Appname;
                    mydial.Commands.Add(new UICommand(
                        "Yes",
                        new UICommandInvokedHandler(this.CommandInvokedHandler_yesclick)));
                    mydial.Commands.Add(new UICommand(
                       "No",
                       new UICommandInvokedHandler(this.CommandInvokedHandler_noclick)));
                    await mydial.ShowAsync();

                }
                else
                {
                    settings.Values["review"] = no;
                }
            }
        }

        private void CommandInvokedHandler_noclick(IUICommand command)
        {

        }

        private async void CommandInvokedHandler_yesclick(IUICommand command)
        {
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            settings.Values["rcheck"] = 1;

            //add your app id here
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + "2735b31a-a443-4b63-9ce6-6adf87ea9291"));
        }
        //void controllerFactory_HLSControllerReady(IHLSControllerFactory sender, IHLSController args)
        //{
        //    controller = args;
        //}
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            HardwareButtons.BackPressed += (o, eventArgs) =>
            {
                Stop_OnClick(null, null);
                player.Source = null;

            };

            GoogleAnalytics.EasyTracker.GetTracker().SendView("MainPage");

            reviewfunction();
            ShowWelcome();



        }

        private async void Ratings_OnClick(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri("ms-windows-store:reviewapp?appid=" + "2735b31a-a443-4b63-9ce6-6adf87ea9291"));
        }

        private void About_OnClick(object sender, RoutedEventArgs e)
        {
            var _aboutPrompt = new AboutPrompt();
            _aboutPrompt.Title = "My Canal 2";
            _aboutPrompt.VersionNumber = "1.1";
            _aboutPrompt.Footer = "Made with flue symptoms in 14h32min " + Environment.NewLine;
            var items = new List<AboutPromptItem>
            {
                new AboutPromptItem {Role = "Dev", AuthorName = "Step4Fun"},
                new AboutPromptItem {Role = "Twitter", WebSiteUrl = "https://www.twitter.com/stefankmg"},

                                new AboutPromptItem
                                {
                                    Role = "Source", WebSiteUrl = "http://www.canal2international.net/"

                                },


            };

            _aboutPrompt.Show(items.ToArray());
        }

        private async void Disclaimer_OnClick(object sender, RoutedEventArgs e)
        {
            var message = @"Please No Copyright Infringements Intended. For viewing purpose only.
All the Content in this app is only Created to enhance the distribution of available resoruce on the internet to the people who search to. This is the claim the author of this app is not the Copyright Owner of any content posted. Owners of the content(Canal 2 International) holds the Orginal proprietorship or designing on the content created.

My Canal 2 contains live streaming videos created from stream source available on the internet in terms to provide the best content  to the comunity. 
This app is being desinged , maintained and posted by the admin individually with no others parties involved.
 If any one founds that we are violating by broadcasting any copyright protected channel, 
Send us a review through the 'Rate' buttons in Settings, 
we will remove the stream from our app on your request.

Built using Canal 2 International : Toujours plus près de vous";
            var prompt = new MessageDialog(message, "Disclaimer");
            prompt.Commands.Add(new UICommand("Close", command => { }));
            await prompt.ShowAsync();


        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            KeepScreenOnRequest.RequestActive();
        }

        private async void Play_OnClick(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Play clicked");

            await LoadStreamingUrl();
        }
        private async Task LoadStreamingUrl()
        {
            var url = string.Empty;
            try
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                if (localSettings.Values.ContainsKey(App.key))
                {
                    var result = localSettings.Values[App.key].ToString();
                    if (!string.IsNullOrWhiteSpace(result))
                    {
                        player.Source = new Uri(result, UriKind.Absolute);
                        player.Play();
                        return;
                    }
                    else
                    {
                        url = await App.GetStreamingUrl();
                    }
                }
                else
                    url = await App.GetStreamingUrl();
                if (!string.IsNullOrWhiteSpace(url))
                {
                    player.Source = new Uri(url, UriKind.Absolute);
                    player.Play();
                    return;
                }


            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
               

            }


        }

        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            player.Stop();
            stop.IsEnabled = false;
            play.IsEnabled = true;
            player.IsPlayPauseVisible = false;

        }


        private void Player_OnPlayerStateChanged(object sender, RoutedPropertyChangedEventArgs<PlayerState> e)
        {
            switch (e.NewValue)
            {
                case PlayerState.Started:
                    {
                        stop.IsEnabled = true;
                        play.IsEnabled = false;
                        player.IsPlayPauseVisible = true;
                        break;
                    }

            }
        }
    }
}
