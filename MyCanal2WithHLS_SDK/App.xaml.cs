﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

using Microsoft.Advertising.WinRT.UI;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace MyCanal2WithHLS_SDK
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
        private TransitionCollection transitions;
        public static string key = "MyCanal2URL";
        public static InterstitialAd MyVideoAd;
        public static readonly string Message = "MyCanal2Message";

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            this.InitializeComponent();
            this.Suspending += this.OnSuspending;
            this.Resuming+=OnResuming;
            //BugSenseHandler.Instance.InitAndStartSession(new ExceptionManager(Current), "bae21a82");

            GetStreamingUrl();
            GetMessage();
        }

        private void OnResuming(object sender, object o)
        {
            
        }

        private void LoadAd()
        {
            var settings = Windows.Storage.ApplicationData.Current.LocalSettings;
            if (settings.Values.Keys.Contains("Old"))
            {
               

            }
        }

        private void CurrentWindow_VisibilityChanged(object sender, VisibilityChangedEventArgs e)
        {
            if (!e.Visible)
            {
                var page = (Window.Current.Content as Frame).Content as MainPage;
                //page?.Clean();
            }
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = true;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                if (e.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // TODO: Load state from previously suspended application
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.VisibilityChanged += CurrentWindow_VisibilityChanged;
            Window.Current.Activate();
        }

        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }
        public static async Task<String> GetMessage()
        {
            HttpClient client = new HttpClient();
            var result = await client.GetStringAsync("http://canal2-step4fun.rhcloud.com/message.php");
            Debug.WriteLine(result);
            var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;

            if (!string.IsNullOrWhiteSpace(result))
            {
                if (localSettings.Values.ContainsKey(App.Message))
                    localSettings.Values[App.Message] = result;
                else
                    localSettings.Values.Add(App.Message, result);
            }
            else
            {
                var old = localSettings.Values[App.Message];
                if (old is string && !string.IsNullOrWhiteSpace(old as string)) result = old as string;

            }
            return result;
        }
        public static async Task<string> GetStreamingUrl()
        {
            HttpClient client = new HttpClient();
            var result = await client.GetStringAsync("http://canal2-step4fun.rhcloud.com/url.php");
            Debug.WriteLine(result);
            if (!string.IsNullOrWhiteSpace(result))
            {
                var localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
                if (localSettings.Values.ContainsKey(App.key))
                    localSettings.Values[App.key] = result;
                else
                    localSettings.Values.Add(App.key, result);


            }
            return result;


        }
        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            var page = (Window.Current.Content as Frame).Content as MainPage;
            //if (page != null) page.Clean();
            deferral.Complete();
        }
    }
}